# Login Application
# Description 
Simple login applictaion using TypeScript, Nest.js, React.js and GraphQL.

**features** 

- Node provides the backend environment for this application
- Nest and GraphQL are used to handle requests
- JWT to handle authentication 
- JSON file to model the application data
- React for displaying UI components
- Material UI for UI design
- GraphQL for data query and mutation
- Context to manage application's state

# Demo Video 

See the application [Link](https://youtu.be/4C8XBde4L0s)

# Install 
Some basic commands are
1. git clone https://gitlab.com/naim.starit/loginapp.git 
2. cd loginapp 
3. yarn
4. yarn installAll  (to install api and web packages) 
(if fetch any problem to install packages, make sure your node version is 16 or upper)


# Run the application 
yarn start 
(this command will start web and api concurrently)

# Language and tools 
- TypeScript
- Node 
- Nest
- GraphQL 
- React 
- Material UI 
- Context 
- JWT


