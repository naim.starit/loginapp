import { JwtAuthGuard } from './gql-authguard';
import { User } from './entities/user.entity';
import { LoginResponse } from './dto/login-response';
import { Args, Mutation, Resolver, Query } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { LoginUserInput } from './dto/login-user.input';
import { NotFoundException, UseGuards } from '@nestjs/common';

@Resolver()
export class AuthResolver {
  constructor(private authService: AuthService) {}
  @UseGuards(JwtAuthGuard)
  @Query(() => [User], { name: 'users' })
  findAll() {
    return this.authService.findAll();
  }
  // find user by userName
  @UseGuards(JwtAuthGuard)
  @Query(() => User, { name: 'user' })
  findOne(@Args('userName') userName: string) {
    const userInfo = this.authService.findOne(userName);
    if (userInfo) return userInfo;
    else throw new NotFoundException();
  }
  // login
  @Mutation(() => LoginResponse)
  login(@Args('loginUserInput') loginUserInput: LoginUserInput) {
    return this.authService.login(loginUserInput);
  }
}
