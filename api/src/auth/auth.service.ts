import { JwtPayload } from './jwt-payload-interface';
import { JwtService } from '@nestjs/jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { LoginUserInput } from './dto/login-user.input';
import { User } from './entities/user.entity';
import { join } from 'path';
import { readFileSync } from 'fs';

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService) {}
  // get path of users.json
  private fileName = join(__dirname, '../../src/db/', 'users.json');
  // read json file and parse users
  private readonly allUser = JSON.parse(readFileSync(this.fileName, 'utf-8'));
  // get all users
  findAll(): User[] {
    return this.allUser;
  }
  // find a user by user name
  findOne(userName: string) {
    const found = this.allUser.find((user) => user.userName === userName);
    return found;
  }
  // login service
  async login(
    loginUserInput: LoginUserInput,
  ): Promise<{ accessToken: string }> {
    const { userName, password } = loginUserInput;
    // find user
    const found: User = this.findOne(userName);
    if (found && found.password === password) {
      const payload: JwtPayload = {
        userName: found.userName,
        userId: found.userId,
        fullName: found.fullName,
      };
      // generate token
      const accessToken = await this.jwtService.sign(payload);
      return { accessToken };
    } else {
      throw new UnauthorizedException();
    }
  }
}
