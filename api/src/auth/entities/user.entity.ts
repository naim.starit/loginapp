import { ObjectType, Field, Int } from '@nestjs/graphql';

@ObjectType()
export class User {
  @Field(() => Int)
  userId: number;

  @Field()
  userName: string;

  @Field()
  password: string;

  @Field()
  fullName: string;
}
