export interface JwtPayload {
  userName: string;
  userId: number;
  fullName: string;
}
