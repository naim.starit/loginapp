import React, { createContext, useReducer } from "react";
import { Action, State } from "../reducer.types";
import { authInitialState, authReducer } from "../reducers";
type AuthContextProviderProps = {
  children: React.ReactChild;
};
export interface IAuthContextProps {
  authState: State;
  authDispatch: React.Dispatch<Action>;
}

export const AuthContext = createContext<IAuthContextProps>({
  authState: authInitialState,
  authDispatch: () => {},
});

export const AuthContextProvider = ({ children }: AuthContextProviderProps) => {
  const [state, dispatch] = useReducer(authReducer, authInitialState);
  return (
    <AuthContext.Provider
      value={{ authState: state, authDispatch: dispatch }}
    >
      {children}
    </AuthContext.Provider>
  );
};
