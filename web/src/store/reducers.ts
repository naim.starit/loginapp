import jwt_decode from 'jwt-decode';
import { State, Action, Info } from './reducer.types';
import {
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGIN_RESET,
  LOGOUT_SUCCESS,
} from "./action-types";
// get token from loal storage
const token = localStorage.getItem("token");
// initial state
export const authInitialState: State = {
  isAuth: !!token,
  userInfo: !!token
    ? jwt_decode<Info>(token)
    : null,
  error: "",
};
// reducer
export const authReducer = (state: State, action: Action): State => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      localStorage.setItem("token", action.payload);
      let info: Info = jwt_decode(action.payload);
      return {
        isAuth: true,
        userInfo: info,
        error: '',
      };
    case LOGIN_RESET:
      return {
        isAuth: false,
        userInfo: null,
        error: '',
      };
    case LOGIN_FAILED:
      return {
        isAuth: false,
        userInfo: null,
        error: action.payload,
      };
    case LOGOUT_SUCCESS:
      localStorage.removeItem("token")
      return {
        isAuth: false,
        userInfo: null,
        error: '',
      };
      default: 
      return state;
  }
};
