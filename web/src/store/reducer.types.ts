export type Info = {
  userName: string,
  userId: number,
  fullName: string,
  iat: number,
  exp: number
}
export type State = {
  isAuth: boolean;
  userInfo: Info | null;
  error: string
};
export type Action =
   { type: "LOGIN_SUCCESS"; payload: string }
  | { type: "LOGIN_FAILED"; payload: string }
  | { type: "LOGOUT_SUCCESS"; payload: null }
  | { type: "LOGIN_RESET"; payload: null };

