import {
  InMemoryCache,
  from,
  ApolloClient,
  createHttpLink,
} from "@apollo/client";
import { onError } from "@apollo/client/link/error";
import { setContext } from "@apollo/client/link/context";
// log graphql errors
const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path }) => {
      console.error(`Graphql error: ${message}`);
    });
  }
});

// create http link
const httpLink = createHttpLink({
  uri: "http://localhost:5001/graphql",
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem("token");
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    },
  };
});
const link = from([errorLink, httpLink]);

// create apollo client instance
export const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: authLink.concat(link),
});
