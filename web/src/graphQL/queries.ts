import { gql } from "@apollo/client";

export const LOAD_USERS = gql`
    query {
        users {
            fullName,
            userName,
            userId
        }
    }
`;

export const LOAD_USER_BY_USERNAME = gql`
  query user($userName: String!) {
    user(userName: $userName) {
      fullName
      userName
      userId
    }
  }
`;