import { gql } from "@apollo/client";

export const LOGIN_MUTATION = gql`
  mutation login($userName: String!, $password: String!) {
    login(loginUserInput: { userName: $userName, password: $password }) {
      accessToken
    }
  }
`;