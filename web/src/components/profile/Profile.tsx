import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { useContext } from "react";
import { AuthContext } from "../../store/context/authContext";
import { LOAD_USER_BY_USERNAME } from "../../graphQL/queries";
import { useQuery } from "@apollo/client";

const useStyles = makeStyles({
  root: {
    maxWidth: 445,
  },
  center: {
    display: "flex",
    justifyContent: "right",
  },
});

interface IUserName {
  user: {
    userName: String,
    userId: Number,
    fullName: String
  };
}

export default function ProfileCard() {
  // context
  const { authState, authDispatch } = useContext(AuthContext);
  const classes = useStyles();
  // fetch logged user data
    const { error, loading, data } = useQuery<IUserName>(
      LOAD_USER_BY_USERNAME,
      {
        variables: { userName: authState.userInfo?.userName },
      }
    );
  const handleLogout = () => {
    authDispatch({type: "LOGOUT_SUCCESS", payload: null})
  }
 
  return (
    <>
      {data && (
        <Card className={classes.root}>
          <CardActionArea>
            <CardMedia
              component="img"
              alt="Contemplative Reptile"
              height="180"
              image="/cover.jpg"
              title="Contemplative Reptile"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                {data?.user.fullName}
              </Typography>
              <Typography gutterBottom variant="h6" component="h5">
                {"ID: "}
                {data?.user.userId}
              </Typography>
              <Typography gutterBottom variant="h6" component="h5">
                {"Username: "} {data?.user.userName}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                Hello {data?.user.fullName}, welcome to profile. If you want to
                logout then please press the logout button bellow.
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions className={classes.center}>
            <Button
              onClick={handleLogout}
              variant="contained"
              size="small"
              color="primary"
            >
              Logout
            </Button>
          </CardActions>
        </Card>
      )}
      {loading && <Typography variant="h2">Loading..........</Typography>}
    </>
  );
}
