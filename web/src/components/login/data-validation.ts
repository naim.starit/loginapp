export const userNameValidate = (userName: string): boolean => {
    if(!!userName && (userName.length >= 4 && userName.length <= 24)) {
        return true;
    }
    else return false;
}
export const passwordValidate = (password: string): boolean => {
    if(!!password && (password.length >= 6 && password.length <= 24)) {
        return true;
    }
    else return false;
}