import { useEffect, useState, useContext } from "react";
// material components
import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import CardHeader from "@material-ui/core/CardHeader";
import Button from "@material-ui/core/Button";
// apollo client
import { LOGIN_MUTATION } from "../../graphQL/mutation";
import { useMutation } from "@apollo/client";
// auth context
import { AuthContext } from "../../store/context/authContext";
// css module
import { useStyles } from "./Login.module";
import { passwordValidate, userNameValidate } from "./data-validation";
import { Typography } from "@material-ui/core";

const LoginComp = () => {
  const classes = useStyles();
  const [userName, setUserName] = useState("");
  const [userNameError, setUserNameError] = useState('');
  const [password, setPassword] = useState("");
  const [passwordError, setPasswordError] = useState('');

  // context
  const {authState, authDispatch} = useContext(AuthContext);

  const [login, { data, loading, error }] = useMutation(LOGIN_MUTATION);
  // error message set 
  const setErrorMessage = () => {
    if (!userNameValidate(userName.trim())) setUserNameError('Username not valid');
    if (!passwordValidate(password.trim())) setPasswordError('Password not valid');
  }
  // validation login input
  const isValid = () => {
    if(userNameValidate(userName.trim()) && passwordValidate(password.trim())) {
      return true;
    }
    else {
      setErrorMessage();
      authDispatch({ type: "LOGIN_RESET", payload: null});
      return false;
    }
  }

  // login data submit handler
  const handleLogin = () => {
    if(isValid()){
      login({
        variables: {
          userName: userName,
          password: password,
        },
      });
    }
  };
// dispatch, if data or error is available
useEffect(()=> {
    if (data) {
      authDispatch({type: 'LOGIN_SUCCESS', payload: data.login.accessToken})
    }
    if (error) {
      authDispatch({type: 'LOGIN_FAILED', payload: 'Unauthorized user'})
    }
}, [data, error, authDispatch])
console.log(authState);

  return (
    <form className={classes.container} noValidate autoComplete="off">
      <Card className={classes.card}>
        <CardHeader className={classes.header} title="Login Application" />
        <CardContent>
          {
            !!authState.error && <Typography color="error">Username or password does not match</Typography>
          }
          <div>
            <TextField
              error={!!userNameError}
              fullWidth
              name="userName"
              type="text"
              label="Username"
              placeholder="Username"
              margin="normal"
              onChange={(e) => setUserName(e.target.value)}
              helperText={userNameError}
            />
            <TextField
              error={!!passwordError}
              fullWidth
              name="password"
              type="password"
              label="Password"
              placeholder="Password"
              margin="normal"
              onChange={(e) => setPassword(e.target.value)}
              helperText={passwordError}
            />
          </div>
        </CardContent>
        <CardActions>
          <Button
            variant="contained"
            size="large"
            color="primary"
            className={classes.loginBtn}
            onClick={handleLogin}
          >
            Login
          </Button>
        </CardActions>
      </Card>
    </form>
  );
};

export default LoginComp;
