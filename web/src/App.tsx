import { ApolloProvider } from "@apollo/client";
// apollo client
import { client } from "./graphQL/client";
// routes
import AllRoutes from "./routes/index";
// context provider
import { AuthContextProvider } from "./store/context/authContext";
import "./App.css";

function App() {
  return (
    <ApolloProvider client={client}>
      <AuthContextProvider>
        <div className="App">
          <AllRoutes />
        </div>
      </AuthContextProvider>
    </ApolloProvider>
  );
}

export default App;
