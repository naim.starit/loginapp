import { useContext } from "react";
import { Route, Redirect, RouteProps } from "react-router-dom";
import { AuthContext } from "../store/context/authContext";

const AuthRoute = (props: RouteProps) => {
  // context
  const { authState } = useContext(AuthContext);
  let isAuth = authState.isAuth;
  if (isAuth) {
    return <Route {...props} />;
  }
  return <Redirect to="/login" />;
};

export default AuthRoute;
