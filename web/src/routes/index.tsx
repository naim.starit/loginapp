import { useContext } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import AuthRoute from "./AuthRoute";
import Profile from "./../pages/profile";
import Login from "../pages/login";
import { AuthContext } from "../store/context/authContext";
const AllRoutes = () => {
  // context
  const { authState } = useContext(AuthContext);
  const isAuth = authState.isAuth;
  return (
    <Router>
      <Switch>
        <AuthRoute exact path="/" component={Profile} />
        {!isAuth && <Route path="/login" component={Login} />}
        <Redirect from="*" to="/" />
      </Switch>
    </Router>
  );
};

export default AllRoutes;
